﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ExtendedFlyCam_Properties
{
    public UnityEngine.EventSystems.EventTrigger[] triggers_onEnter;
    public UnityEngine.EventSystems.EventTrigger[] triggers_onExit;
}

public class ExtendedFlyCam : MonoBehaviour
{

    /*
    EXTENDED FLYCAM
        Desi Quintans (CowfaceGames.com), 17 August 2012.
        Based on FlyThrough.js by Slin (http://wiki.unity3d.com/index.php/FlyThrough), 17 May 2011.
 
    LICENSE
        Free as in speech, and free as in beer.
 
    FEATURES
        WASD/Arrows:    Movement
                  Q:    Climb
                  E:    Drop
                      Shift:    Move faster
                    Control:    Move slower
                        End:    Toggle cursor locking to screen (you can also press Ctrl+P to toggle play mode on and off).
    */

    public ExtendedFlyCam_Properties properties = new ExtendedFlyCam_Properties();
    public float cameraSensitivity = 90;
    public float climbSpeed = 4;
    public float normalMoveSpeed = 10;
    public float slowMoveFactor = 0.25f;
    public float fastMoveFactor = 3;

    public float rotationX = 539;
    public float rotationY = -50;

    float zoom_value = 60;
    public float speed_zoom;
    public float max_zoom = 130;
    public float min_zoom = 15;

    Camera cam;
    public UnityEngine.UI.RawImage[] rimages;

    void OnEnable()
    {
        apply_events_to_triggers();
    }

    void Start()
    {
        cam = GetComponent<Camera>();
    }

    /// <summary>
    /// We need to stay responsible for raw images that wish to display our camera.
    /// When switching to a clone, THIS is what manages the switch in "ownership".
    /// </summary>
    void apply_events_to_triggers()
    {

        //
        foreach (UnityEngine.EventSystems.EventTrigger trigger in properties.triggers_onEnter)
        {
            UnityEngine.EventSystems.EventTrigger.Entry entry = new UnityEngine.EventSystems.EventTrigger.Entry();
            entry.eventID = UnityEngine.EventSystems.EventTriggerType.PointerEnter;
            entry.callback.AddListener((eventData) => { MouseIsInImage_YES(); });

            trigger.delegates.Clear();
            trigger.delegates.Add(entry);
        }

        //
        foreach (UnityEngine.EventSystems.EventTrigger trigger in properties.triggers_onEnter)
        {
            UnityEngine.EventSystems.EventTrigger.Entry entry = new UnityEngine.EventSystems.EventTrigger.Entry();
            entry.eventID = UnityEngine.EventSystems.EventTriggerType.PointerEnter;
            entry.callback.AddListener((eventData) => { MouseIsInImage_YES(); });

            trigger.delegates.Clear();
            trigger.delegates.Add(entry);
        }
    }

    void turn()
    {
        //  Rotate.
        //
        if (!Input.GetMouseButton(0))
            return;
        rotationX += Input.GetAxis("Mouse X") * cameraSensitivity * Time.deltaTime;
        rotationY += Input.GetAxis("Mouse Y") * cameraSensitivity * Time.deltaTime;
        rotationY = Mathf.Clamp(rotationY, -90, 90);

        transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
        transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);

    }
    void translate()
    {
        //  Translate.
        //
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            transform.position += transform.forward * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
            transform.position += transform.right * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            transform.position += transform.forward * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
            transform.position += transform.right * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
        }
        else
        {
            transform.position += transform.forward * normalMoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
            transform.position += transform.right * normalMoveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
        }


        if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * climbSpeed * Time.deltaTime; }
        if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * climbSpeed * Time.deltaTime; }
    }
    void zoom()
    {
        float delta = -Input.mouseScrollDelta.y;
        zoom_value += delta * Time.fixedDeltaTime * speed_zoom;
        zoom_value = Mathf.Clamp(zoom_value, min_zoom, max_zoom);

        if (cam == null)
            return;
        cam.fieldOfView = zoom_value;
    }

    bool bMouseIsInImage = false;
    public void MouseIsInImage_YES() { bMouseIsInImage = true; }
    public void MouseIsInImage_NO() { bMouseIsInImage = false; }
    bool is_in_image()
    {
        if (rimages == null || rimages.Length == 0)
            return true;
        return bMouseIsInImage;
    }

    void Update()
    {
        //
        if (!is_in_image())
            return;

        turn();
        translate();
        zoom();


        //if (Input.GetKeyDown(KeyCode.End))
        //{
        //    Screen.lockCursor = (Screen.lockCursor == false) ? true : false;
        //}
    }
}