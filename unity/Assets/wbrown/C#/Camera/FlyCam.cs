﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class FlyCam_Properties
{
    public float speed_turn;
    public float speed_look_up;
    public float speed_pan;
    public float speed_zoom;


    public float max_look_up;
    public float min_look_up;
    public float max_zoom;
    public float min_zoom;
}

public class FlyCam : UMonoBehavior {

    public FlyCam_Properties properties = new FlyCam_Properties();

    Camera cam;

    float zoom_value;
    public float turn_value;
    public float look_up_value;
    Vector3 translate_value;

    Vector2 mouse_delta;
    Vector3 previous_mouse;

    protected override void Awake_3()
    {
        base.Awake_3();

        cam = GetComponent<Camera>();
    }

    void Update() {

        //
        zoom(-Input.mouseScrollDelta.y);

        //
        {
            mouse_delta = Input.mousePosition - previous_mouse;
            previous_mouse = Input.mousePosition;
            if (Input.GetMouseButton(0))
            {
                turn(mouse_delta.x);
                look_up(mouse_delta.y);
            }
        }

        //
        {
            float rate = Time.fixedDeltaTime * properties.speed_pan;
            float dx = 0;
            float dz = 0;
            dx = Input.GetAxisRaw("Horizontal") * properties.speed_pan;
            dz = Input.GetAxisRaw("Vertical") * properties.speed_pan;
            if (value_too_small(dx))
                dx = 0;
            if (value_too_small(dz))
                dz = 0;
            //translate(dx, dz);
        }

        //
        apply_adjustments();
    }

    void turn(float delta)
    {
        //
        turn_value += delta * Time.fixedDeltaTime * properties.speed_turn;
    }
    void look_up(float delta)
    {
        //
        look_up_value += delta * Time.fixedDeltaTime * properties.speed_look_up;
        look_up_value = Mathf.Clamp(look_up_value, properties.min_look_up, properties.max_look_up);
    }
    void zoom(float delta) {

        //
        zoom_value += delta * Time.fixedDeltaTime * properties.speed_zoom;
        zoom_value = Mathf.Clamp(zoom_value, properties.min_zoom, properties.max_zoom);
    }
    void translate(float deltax, float deltaz)
    {
        translate_value.x += deltax;
        translate_value.z += deltaz;

        //
        Vector3 tr = projected_subject_right() * translate_value.x;
        Vector3 tf = projected_subject_forward() * translate_value.z;
        Vector3 new_pos = (tr + tf).normalized;
        new_pos.y = transform.position.y;
        transform.position = new_pos;
    }

    void apply_adjustments()
    {
        if (cam == null)
            return;

        //
        transform.localRotation = Quaternion.Euler(look_up_value, turn_value, transform.localRotation.z);

        //
        cam.fieldOfView = zoom_value;
    }

    #region utility

    protected Vector3 projected_subject_forward()
    {
        Vector3 original = transform.forward;
        return new Vector3(original.x, 0, original.z);
    }
    protected Vector3 projected_subject_right()
    {
        Vector3 original = transform.right;
        return new Vector3(original.x, 0, original.z);
    }

    float DEFINE_EPSILON { get { return 0.0001f; } }
    bool value_too_small(float value)
    {
        return (Mathf.Abs(value) < DEFINE_EPSILON);
    }
    bool value_too_small(float value, float epsilon)
    {
        return (Mathf.Abs(value) < epsilon);
    }

    #endregion
}
