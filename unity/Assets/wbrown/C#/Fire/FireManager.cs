﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public enum FireStyle{
    Up,
    Across,
    Spread
}

[System.Serializable]
public class FireManager_Properties
{
    public HexGrid grid;
    public MSP_ParticleSystem particles;
    public Vector2[] cell_origins;

    public float delay_turn;

    public FireStyle start_style = FireStyle.Spread;

    public int up_across_width;
}

public class FireManager : UMonoBehavior {

    public FireManager_Properties properties = new FireManager_Properties();
    HexGrid_Explorer explorer = new HexGrid_Explorer();
    List<HexCell> cells_to_burn = new List<HexCell>();
    List<HexCell> cells_to_see = new List<HexCell>();
    List<HexCell> cells_to_burn_next = new List<HexCell>();

    FireStyle style;

    protected override void Start_1()
    {
        base.Start_1();
        properties.delay_turn = Mathf.Clamp(properties.delay_turn, 0.2f, 5.0f);
        InvokeRepeating("execute_turn", properties.delay_turn, properties.delay_turn);

        //  Manually start off our fire!
        //
        int q = 0;
        int r = 0;
        foreach (Vector2 point in properties.cell_origins)
        {
            q = (int)point.x;
            r = (int)point.y;
            HexCell progenitor = properties.grid.Cell(q,r);
            progenitor.Fuel_Mod_Override(1.0f);
            cells_to_burn_next.Add(progenitor);
            Vector3[] points = cells_to_transforms(cells_to_burn_next);
            properties.particles.manually_replace_spawn_points(points);
        }

        //
        //properties.particles.transform.position = properties.grid.Cell(q,r).transform.position;
        properties.particles.gameObject.SetActive(true);
        properties.particles.GetComponent<ParticleSystem>().Play();

        style = properties.start_style;
    }

    void execute_turn() {

        if (WorldPauseState.Instance().properties.bPaused)
            return;

        switch (style)
        {
            case FireStyle.Up: expand_neighbors(); break;
            case FireStyle.Across: expand_neighbors(); break;
            case FireStyle.Spread: expand_neighbors(); break;
        }

    }

    #region expand

    void expand_neighbors()
    {
        //
        ULogger.Instance().write(ULogger_Title.GAMELOGIC, "FireManager.expand_neighbors");

        //  Find all NEW neighbors.
        //
        foreach (HexCell cell in cells_to_burn_next)
        {
            HexCell[] neighbors = explorer.GetNeighbors(properties.grid, cell);
            foreach (HexCell neighbor in neighbors)
            {
                //  Is this a cell we haven't burnt yet?
                //
                if (!neighbor.BurnVisited && !cells_to_burn.Contains(neighbor))
                {
                    cells_to_burn.Add(neighbor);
                }

                //  Is this a new cell?
                //
                else if (!neighbor.Visited)
                {
                    neighbor.Visit();
                    cells_to_see.Add(neighbor);
                }
            }
        }

        //
        Vector3[] points = cells_to_transforms(cells_to_see);
        properties.particles.manually_replace_spawn_points(points);

        //  Burn cells.
        //
        foreach (HexCell cell in cells_to_burn)
        {
            //  Spread this cells fire to the new cell.
            //
            float health = cell.Fire_Health;
            cell.spread_fire(health);
        }


        //  Instead of searching every EVERY (lol) cell of the sim each time we step the fire spreading...
        //  ... let's only expand on the newest ones (aka: the most recent neighbors found).
        //
        cells_to_burn_next.Clear();
        cells_to_burn_next.AddRange(cells_to_burn);
        cells_to_burn.Clear();
        cells_to_see.Clear();
    }

    #endregion


    List<Vector3> temp_points = new List<Vector3>();
    Vector3[] cells_to_transforms(List<HexCell> cells){
    //{
    //    Vector3[] points = new Vector3[cells.Count];
    //    for (int ii = 0; ii < points.Length; ++ii)
    //        points[ii] = cells[ii].transform.position;
    //    return points;

        temp_points.Clear();
        foreach (HexCell cell in cells)
        {
            if (cell.Chunked)
                temp_points.Add(cell.transform.position);
        }
        return temp_points.ToArray();
    }


}
