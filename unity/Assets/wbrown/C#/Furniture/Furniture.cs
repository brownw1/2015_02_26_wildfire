﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections;

[System.Serializable]
public class Furniture_Properties
{
    public int max_health = 1;
    public float fuel_mod = 0;
}

/// <summary>
/// Can be placed on a "hex grid", auto-discovered by the grid cells.
/// </summary>
public class Furniture : UMonoBehavior {

    BoxCollider[] colliders = null;
    Material[] materials;

    public BoxCollider[] Colliders { get { return colliders; } }

    public Furniture_Properties properties = new Furniture_Properties();

    protected override void Awake_3()
    {
        base.Awake_3();

        init();
    }

    public void init()
    {
        colliders = GetComponents<BoxCollider>();
        if (GetComponent<Renderer>())
        materials = GetComponent<Renderer>().materials;
    }

    //
    public void set_color_to_normal()
    {
        Color c = SimulationLibrary.Instance().properties.furniture_color_normal;
        foreach(Material m in materials)
            m.color = c;
    }
    public void set_color_to_burnt()
    {
        Color c = SimulationLibrary.Instance().properties.furniture_color_burnt;
        foreach (Material m in materials)
            m.color = c;
    }
}
