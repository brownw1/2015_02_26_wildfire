﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class HexCell_Properties
{
    public float fuel_mod = 0;

    public float fire_health=1;

}

public class HexCell : UMonoBehavior
{

    HexDefinition definition;
    List<Furniture> furniture = new List<Furniture>();
    bool bVisited = false;
    bool bBurnVisited = false;
    bool bChunked = false;
    int visit_turns = 0;

    new BoxCollider collider;

    HexCell_Properties properties = new HexCell_Properties();
    public void Fuel_Mod_Override(float value) { properties.fuel_mod = value; }
    public float Fire_Health { get { return properties.fire_health; } }
    public void Set_Chunked(bool value) { bChunked = value; }

    //
    public bool Visited { get { return bVisited; } }
    public bool BurnVisited { get { return bBurnVisited; } }
    public bool Chunked { get { return bChunked; } }

    public void Visit() { bVisited = true; }

    //
    float DEFINE_MINIMUM_FIRE_HEALTH_SPREAD { get { return 1.0f; } }

    //
    float Random_Fire_Spread_Chance { get { return 0.8f; } }
    bool Random_Fire_Spread_Success { get { return (UnityEngine.Random.value > Random_Fire_Spread_Chance); } }
    float Random_Fire_Health_Range { get { return 0.5f; } }
    float Random_Fire_Health_Mod { get { return UnityEngine.Random.Range(-Random_Fire_Health_Range, Random_Fire_Health_Range); } }

    //
    public int Index_Across { get { return definition.Coordinates.Q; } }
    public int Index_Up { get { return definition.Coordinates.R; } }

    //
    public HexCoordinates Neighbor_NE { get { return definition.get_neighbor(HexDirection.NE); } }
    public HexCoordinates Neighbor_E { get { return definition.get_neighbor(HexDirection.E); } }
    public HexCoordinates Neighbor_SE { get { return definition.get_neighbor(HexDirection.SE); } }
    public HexCoordinates Neighbor_SW { get { return definition.get_neighbor(HexDirection.SW); } }
    public HexCoordinates Neighbor_W { get { return definition.get_neighbor(HexDirection.W); } }
    public HexCoordinates Neighbor_NW { get { return definition.get_neighbor(HexDirection.NW); } }


    public void initialize(HexGrid grid, int q, int r)
    {
        definition = new HexDefinition(grid, transform.position, q, r);
        collider = GetComponent<BoxCollider>();
    }

    public void add_furniture(Furniture f) {
        if (!furniture.Contains(f))
        {
            //
            furniture.Add(f);
            //string msg = string.Format("HexCell.add_furniture: cell = {0}, furniture = {1}", name, f.name);
            //ULogger.Instance().write(ULogger_Title.GAMELOGIC, msg);
        }
    }

    public bool collide_box(BoxCollider other)
    {
        return collider.bounds.Intersects(other.bounds);
    }

    public bool spread_fire(float fire_health)
    {
        //
        if (bBurnVisited)
        {
            ++visit_turns;
            if (visit_turns >= SimulationLibrary.Instance().properties.visit_cooldown_turns)
            {
                bBurnVisited = false;
                visit_turns = 0;
            }

            else
            {
                bBurnVisited = true;
                return false;
            }
        }


        //
        if (!Random_Fire_Spread_Success)
            return false;


        //  The furniture contributes to our fuel mod.
        //
        float fuel_mod = properties.fuel_mod;
        foreach (Furniture ff in furniture)
            fuel_mod += ff.properties.fuel_mod;

        //
        properties.fire_health = fire_health + fuel_mod + Random_Fire_Health_Mod;
        if (properties.fire_health < DEFINE_MINIMUM_FIRE_HEALTH_SPREAD)
            return false;

        //
        burn_notice();
        return true;
    }
    void burn_notice()
    {

        //string msg = string.Format("HexCell.burn_notice: cell = {0}", name);
        //ULogger.Instance().write(ULogger_Title.GAMELOGIC, msg);

        //
        GetComponent<MeshRenderer>().material.color = Color.black;

        //
        definition.Grid.properties.sinstance.properties.stamper.add_to_queue(new TerrainStamp_Order(transform.position, 1));

        //
        foreach (Furniture f in furniture)
            f.set_color_to_burnt();
    }
}
