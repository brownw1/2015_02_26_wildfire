﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections.Generic;

public class HexChunk : UMonoBehavior {

    private static float DEFINE_RAYCAST_DISTANCE { get { return 500f; } }

    List<HexCell> cells = new List<HexCell>();
    GameObject cells_parent;
    new BoxCollider collider;

    public void initialize( float size)
    {
        cells_parent = new GameObject("cells");
        cells_parent.transform.parent = transform;

        gameObject.AddComponent<Rigidbody>();
        GetComponent<Rigidbody>().isKinematic = true;
        collider = gameObject.AddComponent<BoxCollider>();
        collider.size = new Vector3(size, size / 4f, size);
        collider.center = Vector3.zero;
    }
    public bool is_empty() { return (cells.Count == 0); }
    public bool in_bounds(Vector3 point)
    {
        return GetComponent<Collider>().bounds.Contains(point);
    }
    public void add_cell(HexCell cell) { 
        cells.Add(cell);
        cell.transform.parent = cells_parent.transform;
        cell.Set_Chunked(true);
    }

    public HexCell raycast_to_cells(Ray ray)
    {
        RaycastHit hit;
        foreach (HexCell cell in cells)
        {
            if (cell.GetComponent<Collider>().Raycast(ray, out hit, DEFINE_RAYCAST_DISTANCE))
            {
                //ULogger.Instance().write(ULogger_Title.INPUT, "HexChunk.raycast_to_cells: hit cell... " + cell.name);
                return cell;
            }
        }

        return null;
    }
    public List<HexCell> collide_with_cells(BoxCollider other)
    {
        //  ...It is not within our chunk.
        //
        if (!collider.bounds.Intersects(other.bounds))
            return null;

        //  Return if any hex cell is intersecting it.
        //
        List<HexCell> hits = new List<HexCell>();
        foreach (HexCell cell in cells){
            if (cell.collide_box(other))
                hits.Add(cell);
        }

        //  No success.
        //
        return hits;
    }
}
