﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections;

public class HexCoordinates
{

    #region members

    //  Axial coordinates.
    int q, r;
    public int Q { get { return q; } }
    public int R { get { return r; } }

    //  Cubic coordinates.
    int x, y, z;
    public int X { get { return x; } }
    public int Y { get { return y; } }
    public int Z { get { return z; } }

    //
    public static HexCoordinates zero { get { return new HexCoordinates(0, 0); } }

    #endregion

    public HexCoordinates()
    {
        Set_Axial(0,0);
    }
    public HexCoordinates(int q, int r)
    {
        Set_Axial(q, r);
    }
    public HexCoordinates(int x, int y, int z)
    {
        Set_Cubic(x, y, z);
    }

    #region get

    public HexCoordinates get_neighbor_coordinate_axial(HexDirection direction)
    {
        //
        switch (direction)
        {
            case HexDirection.NONE: return HexCoordinates.zero;

            case HexDirection.NE: return new HexCoordinates(q + 1, r + 1);
            case HexDirection.E: return new HexCoordinates(q + 1, r);
            case HexDirection.SE: return new HexCoordinates(q + 1, r - 1);

            case HexDirection.SW: return new HexCoordinates(q - 1, r - 1);
            case HexDirection.W: return new HexCoordinates(q - 1, r);
            case HexDirection.NW: return new HexCoordinates(q - 1, r + 1);
        }

        //
        return HexCoordinates.zero;
    }
    public HexCoordinates get_neighbor_coordinate_cubic(HexDirection direction)
    {
        //
        switch (direction)
        {
            case HexDirection.NONE: return HexCoordinates.zero;

            case HexDirection.NE: return new HexCoordinates(x + 1, y, z + 1);
            case HexDirection.E: return new HexCoordinates(x + 1, y - 1, z);
            case HexDirection.SE: return new HexCoordinates(x, y - 1, z - 1);

            case HexDirection.SW: return new HexCoordinates(x - 1, y, z - 1);
            case HexDirection.W: return new HexCoordinates(x - 1, y + 1, z);
            case HexDirection.NW: return new HexCoordinates(x, y + 1, z + 1);
        }

        //
        return HexCoordinates.zero;
    }

    #endregion

    #region set

    //
    public void Set_Axial(int q, int r)
    {
        this.q = q;
        this.r = r;
        convert_update_axial_to_cubic();
    }
    public void Set_Cubic(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        convert_update_cubic_to_axial();
    }
    #endregion

    #region convert

    //
    void convert_update_axial_to_cubic()
    {
        x = q;
        z = r;
        y = -x - y;
    }
    void convert_update_cubic_to_axial()
    {
        q = x;
        r = z;
    }
    #endregion

}
