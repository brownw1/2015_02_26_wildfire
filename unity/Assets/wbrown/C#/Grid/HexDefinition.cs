﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections;


public enum HexDirection
{
    //
    NONE,   // 0

    //
    NE,     //  1
    E,      //  2
    SE,     //  3

    //
    SW,     //  4
    W,      //  5
    NW      //  6
}

public class HexDefinition
{

    #region members

    HexGrid grid;
    public HexGrid Grid { get { return grid; } }

    Vector3 center;
    public Vector3 Center { get { return center; } }

    HexCoordinates coordinates = new HexCoordinates();
    public HexCoordinates Coordinates { get { return coordinates; } }

    #endregion

    public HexDefinition(HexGrid grid, Vector3 center, int q, int r)
    {
        this.grid = grid;
        this.center = center;
        coordinates.Set_Axial(q, r);
    }

    #region get
    public static int HexDirection_To_Int(HexDirection dir)
    {
        //
        switch (dir)
        {
            case HexDirection.NONE: return 0;
            case HexDirection.NE: return 1;
            case HexDirection.E: return 2;
            case HexDirection.SE: return 3;
            case HexDirection.SW: return 4;
            case HexDirection.W: return 5;
            case HexDirection.NW: return 6;
        }

        //
        return 0;
    }

    public HexCoordinates get_neighbor(HexDirection direction) {
        return coordinates.get_neighbor_coordinate_axial(direction);
    }

    #endregion

    #region set

    public void set_neighbor(HexDirection direction, int q, int r)
    {
        int index = HexDefinition.HexDirection_To_Int(direction);
    }

    #endregion
}
