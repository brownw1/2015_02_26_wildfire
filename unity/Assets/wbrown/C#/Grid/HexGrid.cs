﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class HexGrid_Properties
{
    public int cells_dimension = 5;
    public float cell_size = 1;
    public int chunks_dimension;
    public float chunk_size;
    public GameObject example;

    public bool debug_click_select = false;

    public GameObject vis_select_cell;
    public GameObject vis_hover_cell;

    public SimulationInstance sinstance;
}
public class HexGrid : UMonoBehavior {

    public HexGrid_Properties properties = new HexGrid_Properties();
    HexChunk[] chunks;
    HexCell[] cells;

    public HexCell Cell(int across, int up){

        //
        if (across < 0 || across >= properties.cells_dimension) return null;
        if (up < 0 || up >= properties.cells_dimension) return null;

        //
        int index = across + up * properties.cells_dimension;
        if(index >= 0 && index < cells.Length)
            return cells[index];

        //
        return null;
    }

    protected override void Awake_3()
    {
        base.Awake_3();
        initialize();
        run_furniture_discovery(properties.sinstance);
    }

    void initialize()
    {

        //
        float c1 = Mathf.Sqrt(3) / 2;
        float cell_height = properties.cell_size * 2;
        float zstep = cell_height * (3f / 4f);
        float cell_width = (Mathf.Sqrt(3f)/2f) * cell_height;
        float xstep = cell_width;
        Vector3 position = Vector3.zero;
        Vector3 min = Vector3.zero;
        Vector3 max = Vector3.zero;

        //  Create chunks.
        //
        chunks = new HexChunk[properties.chunks_dimension * properties.chunks_dimension];
        ULogger.Instance().write(ULogger_Title.GAMELOGIC, string.Format("HexGrid.initialize: creating _{0}_ chunks...", chunks.Length));
        for (int up = 0; up < properties.chunks_dimension; ++up){
            for (int across = 0; across < properties.chunks_dimension; ++across) {

                //
                GameObject go = new GameObject(string.Format("chunk _{0}_{1}_",across,up));
                HexChunk chunk = go.AddComponent<HexChunk>();
                chunks[across + properties.chunks_dimension * up] = chunk;

                //
                chunk.transform.parent = transform;
                position.x = properties.chunk_size * across;
                position.z = properties.chunk_size * up;
                chunk.transform.position = position + transform.position;

                //
                chunk.initialize(properties.chunk_size);
            }
        }

        //  Create tiles.
        //
        bool stagger = false;
        cells = new HexCell[properties.cells_dimension * properties.cells_dimension];
        ULogger.Instance().write(ULogger_Title.GAMELOGIC, string.Format("HexGrid.initialize: creating _{0}_ cells...", properties.cells_dimension * properties.cells_dimension));
        for (int up = 0; up < properties.cells_dimension; ++up){
            for (int across = 0; across < properties.cells_dimension; ++across)
            {

                //
                GameObject go = (GameObject)Instantiate(properties.example);
                HexCell cell = go.GetComponent<HexCell>();
                cell.name = string.Format("cell _{0}_{1}_",across,up);

                //
                cells[across + up * properties.cells_dimension] = cell;

                //
                if (!stagger)
                    position.x = xstep * across;
                else
                    position.x = (xstep * across) + (xstep * 0.5f);
                position.z = zstep * up;
                cell.transform.parent = transform;
                cell.transform.position = position + transform.position;

                //
                foreach (HexChunk chunk in chunks){
                    if (chunk.in_bounds(cell.transform.position)){
                        chunk.add_cell(cell);
                    }
                }

                if (!cell.Chunked)
                {
                    Destroy(cell.gameObject);
                    continue;
                }

                //
                cell.initialize(this, across, up);
            }
            stagger = !stagger;
        }

        //  Delete empty chunks.
        //
        List<HexChunk> non_empty_chunks = new List<HexChunk>();
        foreach (HexChunk chunk in chunks)
        {
            if (!chunk.is_empty())
                non_empty_chunks.Add(chunk);
            else
                Destroy(chunk.gameObject);
        }
        chunks=non_empty_chunks.ToArray();
    }

    #region furniture

   public void run_furniture_discovery(SimulationInstance sinstance)
    {
        //  Check each of the furniture.
        //
        List<Furniture> furniture = sinstance.Furniture;
        foreach (Furniture f in furniture)
        {
            f.init();
            if (f.Colliders == null){
                string msg = string.Format("HexGrid.run_furniture_discovery: no BoxCollider(s) on furniture _{0}", f.name);
                ULogger.Instance().write(ULogger_Title.GAMELOGIC, msg);
                continue;
            }

            //  Collide with each chunk.
            //  Add the furniture to the cell if applicable.
            //
            foreach (HexChunk chunk in chunks){
                foreach (BoxCollider co in f.Colliders){
                    List<HexCell> cells = chunk.collide_with_cells(co);
                    if (cells != null)
                    {
                        foreach (HexCell cell in cells)
                            cell.add_furniture(f);
                    }
                }
            }
        }

    }

    #endregion
}
