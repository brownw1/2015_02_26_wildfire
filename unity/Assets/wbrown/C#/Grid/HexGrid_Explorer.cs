﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections.Generic;

public class HexGrid_Explorer {

    public HexCell[] GetAcross(HexGrid grid, HexCell origin)
    {
        //
        List<HexCell> cells = new List<HexCell>();

        //  Get every cell "across" from the origin cell.
        //
        int up = origin.Index_Up;
        for (int ii = 0; ii < grid.properties.cells_dimension; ++ii)
        {

            //
            cells.Add(grid.Cell(ii, up));
        }

        //  SUCCESS.
        //  Copy the result.
        //
        if (cells.Count > 0)
        {
            return cells.ToArray();
        }

        //  FAILURE.
        //
        return null;
    }
    public HexCell[] GetUp(HexGrid grid, HexCell origin)
    {
        //
        List<HexCell> cells = new List<HexCell>();

        //  Get every cell "up" from the origin cell.
        //
        int across = origin.Index_Across;
        for (int ii = 0; ii < grid.properties.cells_dimension; ++ii)
        {

            //
            cells.Add(grid.Cell(across, ii));
        }

        //  SUCCESS.
        //  Copy the result.
        //
        if (cells.Count > 0)
        {
            return cells.ToArray();
        }

        //  FAILURE.
        //
        return null;
    }


    public HexCell[] GetNeighbors(HexGrid grid, HexCell origin)
    {
        //
        List<HexCell> cells = new List<HexCell>();

        //  Get every cell touching the origin cell.
        //
        HexCell ne = grid.Cell(origin.Neighbor_NE.Q, origin.Neighbor_NE.R);
        if (ne) cells.Add(ne);
        HexCell e = grid.Cell(origin.Neighbor_E.Q, origin.Neighbor_E.R);
        if (e) cells.Add(e);
        HexCell se = grid.Cell(origin.Neighbor_SE.Q, origin.Neighbor_SE.R);
        if (se) cells.Add(se);
        HexCell sw = grid.Cell(origin.Neighbor_SW.Q, origin.Neighbor_SW.R);
        if (sw) cells.Add(sw);
        HexCell w = grid.Cell(origin.Neighbor_W.Q, origin.Neighbor_W.R);
        if (w) cells.Add(w);
        HexCell nw = grid.Cell(origin.Neighbor_NW.Q, origin.Neighbor_NW.R);
        if (nw) cells.Add(nw);

        //  SUCCESS.
        //  Copy the result.
        //
        if (cells.Count > 0)
        {
            return cells.ToArray();
        }

        //  FAILURE.
        //
        return null;
    }
}
