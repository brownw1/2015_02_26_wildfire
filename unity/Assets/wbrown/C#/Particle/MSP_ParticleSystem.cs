﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class MSP_ParticleSystem_Properties
{
    public float rate_cycle;
    public float random_offset_distance_max = 10;
    public Transform[] spawn_points;
}

/// <summary>
/// MSP = (M)ULTIPLE (S)PAWN (P)OINTS
/// ATTEMPTS to support specific + "multiple spawn points" for a particle
/// system by randomly cycling the system through an array of locations.
/// System simulates in "world" space.
/// </summary>
public class MSP_ParticleSystem : UMonoBehavior
{

    #region members

    public MSP_ParticleSystem_Properties properties = new MSP_ParticleSystem_Properties();
    HashSet<Vector3> spawn_points_hash = new HashSet<Vector3>();
    List<Vector3> spawn_points_list = new List<Vector3>();
    ParticleSystem psystem;
    ParticleSystem.Particle[] particles;
    int index_particle = 0;

    #endregion

    protected override void Start_1()
    {
        //
        base.Start_1();

        //
        foreach (Transform t in properties.spawn_points)
        {
            spawn_points_hash.Add(t.position);
            spawn_points_list.Add(t.position);
        }

        //
        psystem = GetComponent<ParticleSystem>();
        psystem.simulationSpace = ParticleSystemSimulationSpace.World;
        psystem.Emit(psystem.maxParticles);
        psystem.Play();

        //
        particles = new ParticleSystem.Particle[psystem.particleCount];

        //
        InvokeRepeating("cycle_positions", properties.rate_cycle, properties.rate_cycle * Time.fixedDeltaTime);
    }

    void Update()
    {
        if (psystem == null)
            return;

        if (WorldPauseState.Instance().properties.bPaused && psystem.isPlaying)
            psystem.Pause();
        else if (!WorldPauseState.Instance().properties.bPaused && !psystem.isPlaying)
            psystem.Play();
    }

    Vector3 get_random_spawn_point()
    {
        int index = UnityEngine.Random.Range(0, spawn_points_list.Count);
        return spawn_points_list[index];
    }
    Vector3 get_random_offset()
    {
        float x = UnityEngine.Random.Range(-properties.random_offset_distance_max, properties.random_offset_distance_max);
        float z = UnityEngine.Random.Range(-properties.random_offset_distance_max, properties.random_offset_distance_max);
        return new Vector3(x, 0, z);
    }
    void cycle_position()
    {
        if (spawn_points_list.Count == 0)
            return;
        transform.position = get_random_spawn_point();
    }

    public void manually_replace_spawn_points(Vector3[] replacement)
    {
        //
        spawn_points_hash.Clear();
        spawn_points_list.Clear();

        //
        foreach (Vector3 t in replacement)
            spawn_points_hash.Add(t);
        spawn_points_list.AddRange(replacement);
    }
    public void manually_join_spawn_points(Vector3[] replacement)
    {

        //
        foreach (Vector3 t in replacement){
            if (!spawn_points_hash.Contains(t)){
                spawn_points_hash.Add(t);
                spawn_points_list.Add(t);
            }
        }
    }

    void cycle_positions()
    {
        if (WorldPauseState.Instance().properties.bPaused)
            return;

        //
        if (spawn_points_list.Count == 0 || psystem == null)
            return;

        {

            //  Random positions.
            //
            //psystem.GetParticles(particles);
            //if (particles[index_particle].lifetime > particles[index_particle].startLifetime * 0.5f)
            //{
            //    //
            //    particles[index_particle].position = get_random_spawn_point();
            //    particles[index_particle].color = Color.white;
            //    //
            //    psystem.SetParticles(particles, particles.Length);
            //    psystem.Play();
            //}
            //if (index_particle >= particles.Length)
            //    index_particle = 0;
            //++index_particle;

            //  Sequential positions.
            //
            //psystem.GetParticles(particles);
            //index_particle = 0;
            //while (index_particle < particles.Length && index_particle < spawn_points_list.Count)
            //{
            //    //
            //    particles[index_particle].position = spawn_points_list[index_particle];
            //    particles[index_particle].color = Color.white;
            //    //
            //    ++index_particle;
            //}
            //psystem.SetParticles(particles, particles.Length);
            //psystem.Play();
            //psystem.Emit(psystem.maxParticles);


            //  Emit positions.
            //
            index_particle = 0;
            while (index_particle < particles.Length && index_particle < spawn_points_list.Count)
            {
                //
                psystem.Emit(spawn_points_list[index_particle] + get_random_offset(), Vector3.zero, psystem.startSize, psystem.startLifetime, psystem.startColor);
                //
                ++index_particle;
            }

        }

    }
}
