﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Persisted_Settings_Affector_Properties_Individual_House
{
    public bool bActive = false;
    public GameObject tree_1, tree_2, tree_3, tree_4, tree_5, tree_6;
}

/// <summary>
/// When you toggle an option, it is written to the "Simulation_Persisted_Settings".
/// This takes those settings and changes the world based on them.
/// </summary>
public class Persisted_Settings_Affector : UMonoBehavior {
    public Persisted_Settings_Affector_Properties_Individual_House properties_individual_house = new Persisted_Settings_Affector_Properties_Individual_House();


    void Update()
    {
        update_individual_house();
        Simulation_Persisted_Settings.Instance().bDirty = false;
    }

    void update_individual_house()
    {
        if (!properties_individual_house.bActive || !Simulation_Persisted_Settings.Instance().bDirty)
            return;

        //
        if (Simulation_Persisted_Settings.Instance().get_value_int(ESimulated_Persisted_Settings_Mode.Individual_House, "tree_1") == 0)
            properties_individual_house.tree_1.SetActive(false);
        else
            properties_individual_house.tree_1.SetActive(true);

        //
        if (Simulation_Persisted_Settings.Instance().get_value_int(ESimulated_Persisted_Settings_Mode.Individual_House, "tree_2") == 0)
            properties_individual_house.tree_2.SetActive(false);
        else
            properties_individual_house.tree_2.SetActive(true);

        //
        if (Simulation_Persisted_Settings.Instance().get_value_int(ESimulated_Persisted_Settings_Mode.Individual_House, "tree_3") == 0)
            properties_individual_house.tree_3.SetActive(false);
        else
            properties_individual_house.tree_3.SetActive(true);

        //
        if (Simulation_Persisted_Settings.Instance().get_value_int(ESimulated_Persisted_Settings_Mode.Individual_House, "tree_4") == 0)
            properties_individual_house.tree_4.SetActive(false);
        else
            properties_individual_house.tree_4.SetActive(true);

        //
        if (Simulation_Persisted_Settings.Instance().get_value_int(ESimulated_Persisted_Settings_Mode.Individual_House, "tree_5") == 0)
            properties_individual_house.tree_5.SetActive(false);
        else
            properties_individual_house.tree_5.SetActive(true);

        //
        if (Simulation_Persisted_Settings.Instance().get_value_int(ESimulated_Persisted_Settings_Mode.Individual_House, "tree_6") == 0)
            properties_individual_house.tree_6.SetActive(false);
        else
            properties_individual_house.tree_6.SetActive(true);
    }
}
