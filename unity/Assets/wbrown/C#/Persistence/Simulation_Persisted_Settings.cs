﻿using UnityEngine;
using System.Collections.Generic;

public enum ESimulated_Persisted_Settings_Mode
{
    Individual_House,
    SideBySide,
    Community,
    Neighborhood
}

public class Simulation_Persisted_Settings : USingleton_Game<Simulation_Persisted_Settings>
{
    public bool bDirty = true;

    string DEFINE_PREFIX_KEY_INDIVIDUAL_HOUSE { get { return "individual_house"; } }
    string DEFINE_PREFIX_KEY_SIDE_BY_SIDE{ get { return "sidebyside"; } }
    string DEFINE_PREFIX_KEY_COMMUNITY { get { return "community"; } }
    string DEFINE_PREFIX_KEY_NEIGHBORHOOD { get { return "neighborhood"; } }

    public string DEFINE_KEY_ZONE_INDICATOR { get { return "zone_indicator"; } }

    protected override void Awake_3()
    {
        base.Awake_3();
        PlayerPrefs.DeleteAll();
    }

    string mod_key(ESimulated_Persisted_Settings_Mode mode, string key)
    {
        switch (mode)
        {
            case ESimulated_Persisted_Settings_Mode.Individual_House:
                return DEFINE_PREFIX_KEY_INDIVIDUAL_HOUSE + key;
            case ESimulated_Persisted_Settings_Mode.SideBySide:
                return DEFINE_PREFIX_KEY_SIDE_BY_SIDE + key;
            case ESimulated_Persisted_Settings_Mode.Community:
                return DEFINE_PREFIX_KEY_COMMUNITY + key;
            case ESimulated_Persisted_Settings_Mode.Neighborhood:
                return DEFINE_PREFIX_KEY_NEIGHBORHOOD + key;
        }
        return key;
    }

    public void set_value(ESimulated_Persisted_Settings_Mode mode, string key, string value)
    {
        key = mod_key(mode, key);
        PlayerPrefs.SetString(key, value);
        bDirty = true;
    }
    public string get_value(ESimulated_Persisted_Settings_Mode mode, string key)
    {
        key = mod_key(mode, key);
        if (!PlayerPrefs.HasKey(key))
            return null;
        return PlayerPrefs.GetString(key);
    }


    public void set_value_int(ESimulated_Persisted_Settings_Mode mode, string key, int value)
    {
        key = mod_key(mode, key);
        PlayerPrefs.SetInt(key, value);
        bDirty = true;
    }
    public int get_value_int(ESimulated_Persisted_Settings_Mode mode, string key)
    {
        key = mod_key(mode, key);
        if (!PlayerPrefs.HasKey(key))
            return 0;
        return PlayerPrefs.GetInt(key);
    }
}
