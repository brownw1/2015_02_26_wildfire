﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/


using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class SimulationInstance_Properties
{
    public HexGrid grid;
    public GameObject fire_managers;
    public GameObject furniture_parent;
    public TerrainStamper stamper;

    public LayerMask mask_show_tiles;
    public LayerMask mask_hide_tiles;

    public bool bUseFire = true;
    public bool bShowTiles = true;
}

public class SimulationInstance : UMonoBehavior {


    public SimulationInstance_Properties properties = new SimulationInstance_Properties();
    List<Furniture> furniture = new List<Furniture>();

    public List<Furniture> Furniture { get { return furniture; } }

    string DEFINE_MASK_NAME_HEXCELLS { get { return "hex tile"; } }

    bool bShowTiles_Prev = true;

    protected override void Awake_3()
    {
        base.Awake_3();

        discover_furniture();
    }

    protected override void Start_1(){
        base.Start_1();

        //
        properties.grid.properties.sinstance = this;
        properties.grid.gameObject.SetActive(true);
    }

    protected override void Start_2(){
        base.Start_2();

        //
        if(properties.bUseFire)
            properties.fire_managers.gameObject.SetActive(true);
    }

    void Update()
    {
        update_visibility_tiles();
    }

    void update_visibility_tiles()
    {
        if (bShowTiles_Prev != properties.bShowTiles)
        {
            bShowTiles_Prev = properties.bShowTiles;

            //  ON.
            //
            if (properties.bShowTiles)
                Camera.main.cullingMask = properties.mask_show_tiles;//|= LayerMask.NameToLayer(DEFINE_MASK_NAME_HEXCELLS);

            //  OFF.
            //
            else
                Camera.main.cullingMask = properties.mask_hide_tiles;//&= ~LayerMask.NameToLayer(DEFINE_MASK_NAME_HEXCELLS);
        }
    }

    //
    void discover_furniture()
    {
        furniture.Clear();
        foreach (Transform child in properties.furniture_parent.transform)
        {
            Furniture f = child.GetComponent<Furniture>();
            if (f)
                furniture.Add(f);
        }

        //
        string msg = string.Format("SimulationInstance.discover_furniture: found = {0}",furniture.Count);
        ULogger.Instance().write(ULogger_Title.SYSTEM, msg);
    }
}
