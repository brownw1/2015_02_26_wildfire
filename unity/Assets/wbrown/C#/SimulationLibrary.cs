﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SimulationLibrary_Properties
{
    public Color furniture_color_normal;
    public Color furniture_color_burnt;

    public int visit_cooldown_turns = 3;
}
public class SimulationLibrary : USingleton_Scene<SimulationLibrary> {
    public SimulationLibrary_Properties properties = new SimulationLibrary_Properties();


}
