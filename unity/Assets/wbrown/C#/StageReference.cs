﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class StageReference_Properties
{
    public GameObject fires;
}
public class StageReference : UMonoBehavior {

    public StageReference_Properties properties = new StageReference_Properties();
}
