﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TerrainStamp_Order
{
    public Vector3 world_position;
    public float size;

    public TerrainStamp_Order(Vector3 pos, float size)
    {
        this.world_position = pos;
        this.size = size;
    }
}

[System.Serializable]
public class Terrain_Properties
{
    public TerrainData terrainData;
    public Terrain terrain;
}

[System.Serializable]
public class TerrainStamper_Properties
{

    public Texture2D stamp_texture;
    public Texture2D combined_stamp_map;
    public RenderTexture render_texture;
    public Texture2D original_control_map;

    public Material stamp_material;

    public Terrain_Properties original;
    public Terrain_Properties cloned;

    public GameObject terrain_trecker;
    public Vector3 visualize_terrain_xz;

    public int max_stamps_per_frame=10;
    public float delay_write = 1.0f;
}

/// <summary>
/// ASSUMPTIONS:
///     (+): The final splat texture on your terrain is the "burnt" texture.
///     (+): The "combined_stamp_map" is the same size as your terrain's alpha map.
///     (+): The alpha map is square.
/// </summary>
public class TerrainStamper : UMonoBehavior {

    public TerrainStamper_Properties properties = new TerrainStamper_Properties();
    Stack<TerrainStamp_Order> stamp_queue = new Stack<TerrainStamp_Order>();
    float[, ,] original_terrain_alphamaps;
    float[, ,] new_terrain_alphamaps;
    Texture2D combined_stamp_map_instance;
    Color32[] src_block;
    bool dirty_write = false;

    void OnEnable()
    {

        Graphics.SetRenderTarget(properties.render_texture);
        GL.Clear(true, true, Color.red);
        dirty_write = true;
    }

    protected override void Start_3()
    {
        base.Start_3();

        //
        CreateTerrain();
        capture_original_terrain_alphamaps();

        //
        //initialize_stamp_map();

        //
        initialize_render_texture_control_map();
        begin_async_writes();
    }

    void Update()
    {
        //update_splat();
    }
    void OnPostRender()
    {
        update_splat_rt();
    }

    #region splat

    void update_splat()
    {
        if (combined_stamp_map_instance != null && stamp_queue.Count > 0)
        {
            compile_combined_stamp_map();
            Splat_To_Terrain();
        }
    }
    void update_splat_rt()
    {
        if (stamp_queue.Count > 0)
        {
            //  Put all stamps on the render texture.
            //
            compile_combined_stamp_map_rt();

            //  Put the render texture on texture2D.
            //
            apply_rt_to_combined_stamp_map();

            //  Put the texture2D into the terrain.
            //
            //apply_rt_combined_stamp_map_to_terrain();
        }
    }

    #endregion

    #region stamps

    public void add_to_queue(TerrainStamp_Order order) { stamp_queue.Push(order); }

    #region cpu

    public void initialize_stamp_map()
    {

        combined_stamp_map_instance = Instantiate<Texture2D>(properties.combined_stamp_map);
        Color[] c = combined_stamp_map_instance.GetPixels(0, 0, combined_stamp_map_instance.width, combined_stamp_map_instance.height);
        for (int ii = 0; ii < c.Length; ++ii)
            c[ii] = Color.clear;
        combined_stamp_map_instance.SetPixels(c);
        combined_stamp_map_instance.Apply();

        //
        src_block = properties.stamp_texture.GetPixels32();
    }

    void compile_combined_stamp_map()
    {
        //  Get pixels ready.
        //
        Color[] pixels_stamp = properties.stamp_texture.GetPixels();

        //
        Graphics.Blit(combined_stamp_map_instance, properties.render_texture);

        //  Centered at each order's position, place the data of the "stamp_texture".
        //
        Vector2 terrain_space_position;
        Vector2 offset = new Vector2(0.5f,0.5f);
        int wh = properties.stamp_texture.width;
        foreach (TerrainStamp_Order order in stamp_queue)
        {
            //
            terrain_space_position = world_to_terrain_space(order.world_position);

            //
            //stamp_additive(properties.stamp_texture, combined_stamp_map_instance, terrain_space_position, offset);
        }
        stamp_queue.Clear();

        //  Apply the change.
        //
        combined_stamp_map_instance.Apply();
    }
    Color32 add_c32(Color32 arg1, Color32 arg2)
    {
        return new Color32(
            (byte)(arg1.r + arg2.r),
            (byte)(arg1.g + arg2.g),
            (byte)(arg1.b + arg2.b),
            (byte)(arg1.a + arg2.a)
            );
    }
    void stamp_additive(Texture2D src, Texture2D dest, Vector2 location, Vector2 offset)
    {

        //
        int offx = (int)(src.width * offset.x);
        int offy = (int)(src.height * offset.y);

        //
        int startx = (int)(dest.width * location.x);
        int starty = (int)(dest.height * location.y);

        //
        int dxfrom = Mathf.Clamp(startx - offx, 0, dest.width);
        int dyfrom = Mathf.Clamp(starty - offy, 0, dest.height);
        int dwidth = (dest.width - (dxfrom+src.width) < 0) ? 0 : src.width;
        int dheight = (dest.height - (dyfrom + src.height) < 0) ? 0 : src.height;

        //  Sample the src/dest block of pixels.
        //
        Color[] dest_block = dest.GetPixels(dxfrom, dyfrom, dwidth, dheight);

        //  Combine them into a final block.
        //
        Color32[] final_block = new Color32[src_block.Length];
        for (int ii = 0; ii < final_block.Length; ++ii)
        {
            if (dest_block.Length > ii)
                final_block[ii] = add_c32(src_block[ii],dest_block[ii]);
            else
                final_block[ii] = src_block[ii];
        }

        //  Write this to the destination and apply it.
        //
        dest.SetPixels32(dxfrom, dyfrom, dwidth, dheight, final_block);
        dest.Apply();
    }

    #endregion

    #region gpu

    public void initialize_render_texture_control_map()
    {
        //  Let our additive stamp material know the stamp image.
        //
        //properties.stamp_material.SetTexture("_MainTex", properties.stamp_texture);

        //  Copy the original control map to the render texture for a clean start.
        //
        Graphics.Blit(properties.original_control_map, properties.render_texture);

        //
        combined_stamp_map_instance = Instantiate<Texture2D>(properties.combined_stamp_map);
    }

    void begin_async_writes()
    {
        InvokeRepeating("make_async_write", properties.delay_write, properties.delay_write);
    }
    void make_async_write()
    {
        //
        if (!dirty_write || !gameObject.activeInHierarchy)
            return;
        StartCoroutine(ASYNC_apply_rt_combined_stamp_map_to_terrain());
    }

    void compile_combined_stamp_map_rt()
    {

        //  Centered at each order's position, place the data of the "stamp_texture".
        //
        Vector2 terrain_space_position;
        Vector2 offset = new Vector2(0.5f,0.6f); //Vector2(1, 0.5f);
        int wh = properties.stamp_texture.width;
        int stamps = 0;
        while (stamp_queue.Count > 0)
        {
            //
            dirty_write = true;

            //
            TerrainStamp_Order order = stamp_queue.Pop();

            //  Find where we will place the stamp.
            //
            terrain_space_position = world_to_terrain_space(order.world_position);

            //  Draw the stamp image to the render texture.
            //
            stamp_rt(properties.stamp_texture, combined_stamp_map_instance, properties.render_texture, terrain_space_position, offset);

            //
            ++stamps;
            if (stamps >= properties.max_stamps_per_frame)
                break;
        }
    }
   
    void stamp_rt(Texture2D src, Texture2D dest, RenderTexture rt, Vector2 location, Vector2 offset)
    {
        //
        int offx = (int)(src.width * offset.x);
        int offy = (int)(src.height * offset.y);

        //
        int startx = (int)(dest.width * location.x);
        int starty = (int)(dest.height * location.y);

        //
        int x = startx - offx;
        int y = starty - offy;

        //
        Graphics.SetRenderTarget(properties.render_texture);
        RenderTexture.active = rt;
        GL.PushMatrix();
        GL.LoadPixelMatrix(0, dest.width, dest.height, 0);
        properties.stamp_material.SetPass(0);
        Graphics.DrawTexture(new Rect(x, y, src.width, src.width), src, properties.stamp_material);
        GL.PopMatrix();
        RenderTexture.active = null;
        Graphics.SetRenderTarget(null);
    }

    void apply_rt_to_combined_stamp_map()
    {
        //  Read the Render Texture into our Texture2D of the combined stamping.
        //
        RenderTexture.active = properties.render_texture;
        combined_stamp_map_instance.ReadPixels(new Rect(0, 0, properties.render_texture.width, properties.render_texture.height), 0, 0);
        combined_stamp_map_instance.Apply();
        RenderTexture.active = null;
    }

    void apply_rt_combined_stamp_map_to_terrain()
    {
        //  Apply the texture to the terrain.
        //
        int num_textures = properties.cloned.terrainData.splatPrototypes.Length;
        float[, ,] map = new float[properties.cloned.terrainData.alphamapWidth, properties.cloned.terrainData.alphamapHeight, num_textures];

        // For each point on the alphamap...
        //
        for (var y = 0; y < properties.cloned.terrainData.alphamapHeight; y++){
            for (var x = 0; x < properties.cloned.terrainData.alphamapWidth; x++){

                //  Get the combined stamp map value at this point.
                //
                float stamp_alpha = combined_stamp_map_instance.GetPixel(x, y).a;

                //  Adjust the map values by the originals.
                //
                for (int ii = 0; ii < num_textures - 1; ++ii)
                {
                    map[x, y, ii] = original_terrain_alphamaps[x, y, ii] - stamp_alpha;
                }

                //  Final map value is the burnt.
                //  This should be full on stamp value.
                //
                map[x, y, num_textures - 1] = stamp_alpha;
            }
        }

        //  Apply our changes.
        //
        properties.cloned.terrainData.SetAlphamaps(0, 0, map);
    }

    IEnumerator ASYNC_apply_rt_combined_stamp_map_to_terrain()
    {

        //  Apply the texture to the terrain.
        //
        int num_textures = properties.cloned.terrainData.splatPrototypes.Length;

        // For each point on the alphamap...
        //
        for (var y = 0; y < properties.cloned.terrainData.alphamapHeight; y++){
            for (var x = 0; x < properties.cloned.terrainData.alphamapWidth; x++){

                //  Get the combined stamp map value at this point.
                //
                float stamp_alpha = combined_stamp_map_instance.GetPixel(x, y).a;

                //  Adjust the map values by the originals.
                //
                for (int ii = 0; ii < num_textures - 1; ++ii)
                {
                    new_terrain_alphamaps[x, y, ii] = original_terrain_alphamaps[x, y, ii] - stamp_alpha;
                }

                //  Final map value is the burnt.
                //  This should be full on stamp value.
                //
                new_terrain_alphamaps[x, y, num_textures - 1] = stamp_alpha;
            }
        }

        //  Apply our changes.
        //
        properties.cloned.terrainData.SetAlphamaps(0, 0, new_terrain_alphamaps);

        //
        dirty_write = false;

        //
        yield return 0;
    }

    #endregion

    #endregion

    #region terrain

    void capture_original_terrain_alphamaps()
    {
        int size = properties.cloned.terrainData.alphamapWidth;
        original_terrain_alphamaps = properties.cloned.terrainData.GetAlphamaps(0, 0, size, size);
        new_terrain_alphamaps = properties.cloned.terrainData.GetAlphamaps(0, 0, size, size);
    }
    
    public void Splat_To_Terrain()
    {
        //
        int num_textures = properties.cloned.terrainData.splatPrototypes.Length;
        float[, ,] map = new float[properties.cloned.terrainData.alphamapWidth, properties.cloned.terrainData.alphamapHeight, num_textures];
		
		// For each point on the alphamap...
        //
        for (var y = 0; y < properties.cloned.terrainData.alphamapHeight; y++){
            for (var x = 0; x < properties.cloned.terrainData.alphamapWidth; x++){
				
                //  Get the combined stamp map value at this point.
                //
                float stamp_alpha = combined_stamp_map_instance.GetPixel(x, y).a;
				
                //  Adjust the map values by the originals.
                //
                for (int ii = 0; ii < num_textures-1; ++ii)
                {
                    map[x, y, ii] = original_terrain_alphamaps[x,y,ii] - stamp_alpha;
                }

                //  Final map value is the burnt.
                //  This should be full on stamp value.
                //
                map[x, y, num_textures-1] = stamp_alpha;
			}
		}
		
        //  Apply our changes.
        //
        properties.cloned.terrainData.SetAlphamaps(0, 0, map);
    }

    public void CreateTerrain()
    {
        //  Copy the splat textures.
        //
        int num_textures = properties.original.terrainData.splatPrototypes.Length;
        SplatPrototype[] tex = new SplatPrototype[num_textures];
        for (int i = 0; i < num_textures; i++)
        {
            tex[i] = new SplatPrototype();
            tex[i].texture = properties.original.terrainData.splatPrototypes[i].texture;    //Sets the texture
            tex[i].tileSize = properties.original.terrainData.splatPrototypes[i].tileSize;  //Sets the size of the texture
        }

        //  Copy the alpha maps.
        //
        int size = properties.original.terrainData.alphamapWidth;
        float[, ,] alphamaps_original = properties.original.terrainData.GetAlphamaps(0, 0, size, size);
        float[, ,] alphamaps_cloned = new float[properties.cloned.terrainData.alphamapWidth, properties.cloned.terrainData.alphamapHeight, num_textures];
        for (var y = 0; y < properties.cloned.terrainData.alphamapHeight; y++){
            for (var x = 0; x < properties.cloned.terrainData.alphamapWidth; x++){
                for (int ii = 0; ii < num_textures; ++ii )
                    alphamaps_cloned[x, y, ii] = alphamaps_original[x, y, ii];
            }
        }
        properties.cloned.terrainData.SetAlphamaps(0, 0, alphamaps_cloned);

        //  Create the new terrain!
        //
        properties.cloned.terrainData.splatPrototypes = tex;
        properties.cloned.terrain = Terrain.CreateTerrainGameObject(properties.cloned.terrainData).GetComponent<Terrain>();

        //  Set it up and disable it.
        //
        properties.cloned.terrain.name = "Terrain - Clone";
        properties.cloned.terrain.transform.parent = transform.parent;
        properties.cloned.terrain.transform.position = properties.original.terrain.transform.position;
        properties.original.terrain.gameObject.SetActive(false);
    }

    #endregion

    #region space

    /// <summary>
    /// Drag an object around and receive a display of it's world position in terrain space!
    /// </summary>
    void dbg_terrain_trecker()
    {
        Vector3 world = properties.terrain_trecker.transform.position;
        Vector3 terrainLocalPos = world - properties.cloned.terrain.transform.position; 
        Vector2 normalizedPos = new Vector2(
            Mathf.InverseLerp(0f, properties.cloned.terrainData.size.x, terrainLocalPos.x),
            Mathf.InverseLerp(0f, properties.cloned.terrainData.size.z, terrainLocalPos.z));


        ///
        properties.visualize_terrain_xz.x = normalizedPos.x;
        properties.visualize_terrain_xz.z = normalizedPos.y;
    }


    Vector2 world_to_terrain_space(Vector3 world)
    {

        //
        Vector3 terrainLocalPos = world - properties.cloned.terrain.transform.position;

        //
        Vector2 normalizedPos = new Vector2(
            Mathf.InverseLerp(0f, properties.cloned.terrainData.size.z, terrainLocalPos.z),
            1f-Mathf.InverseLerp(0f, properties.cloned.terrainData.size.x, terrainLocalPos.x));

        //
        return normalizedPos;
    }

    #endregion
}
