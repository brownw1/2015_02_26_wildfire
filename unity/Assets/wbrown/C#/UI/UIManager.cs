﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class UIManager_Properties_Stages
{
    public StageReference individual_house;
    public StageReference individual_house_CLONE;

    public StageReference sbs_left;
    public StageReference sbs_left_CLONE;

    public StageReference sbs_right;
    public StageReference sbs_right_CLONE;

    public StageReference community;
    public StageReference community_CLONE;

    public StageReference neighborhood;
    public StageReference neighborhood_CLONE;

    public void disable_originals()
    {
        individual_house.gameObject.SetActive(false);
        sbs_left.gameObject.SetActive(false);
        sbs_right.gameObject.SetActive(false);
        community.gameObject.SetActive(false);
        neighborhood.gameObject.SetActive(false);
    }
    public void enable_originals()
    {
        individual_house.gameObject.SetActive(true);
        sbs_left.gameObject.SetActive(true);
        sbs_right.gameObject.SetActive(true);
        community.gameObject.SetActive(true);
        neighborhood.gameObject.SetActive(true);
    }
}

[System.Serializable]
public class UIManager_Properties_Prompt
{
    public GameObject panel;
}

[System.Serializable]
public class UIManager_Properties_Panel_Mitigate_Individual_House
{
    public GameObject panel;
    public GameObject panel_state_1;
    public GameObject panel_state_2;
    public GameObject panel_options_maximized;
    public GameObject panel_options_minimized;

    public UnityEngine.UI.Button btn_play;
    public UnityEngine.UI.Button btn_pause;

    public bool bZoneIndicator = false;
}
[System.Serializable]
public class UIManager_Properties_Panel_Setup_Community
{
    public GameObject panel;
    public GameObject panel_state_1;
    public GameObject panel_state_2;
    public GameObject panel_options_maximized;
    public GameObject panel_options_minimized;

    public UnityEngine.UI.Button btn_play;
    public UnityEngine.UI.Button btn_pause;
}
[System.Serializable]
public class UIManager_Properties_Panel_Setup_Neighborhood
{
    public GameObject panel;
    public GameObject panel_state_1;
    public GameObject panel_state_2;
    public GameObject panel_options_maximized;
    public GameObject panel_options_minimized;

    public UnityEngine.UI.Button btn_play;
    public UnityEngine.UI.Button btn_pause;
}
[System.Serializable]
public class UIManager_Properties_Panel_Front_Individual_House
{
    public GameObject panel_trees;
    public GameObject panel_siding;
    public GameObject panel_roof;
    public GameObject panel_starting_objects;

    public UnityEngine.UI.Button btn_trees;
    public UnityEngine.UI.Button btn_siding;
    public UnityEngine.UI.Button btn_roof;
    public UnityEngine.UI.Button btn_starting_objects;

    public GameObject parent_toggle_trees;
}
[System.Serializable]
public class UIManager_Properties_Panel_Front_Side_By_Side
{
    public GameObject panel_settings;
    public GameObject panel_simulate;
}
[System.Serializable]
public class UIManager_Properties_Panel_Front_Community
{
    public GameObject panel_setup;
    public GameObject panel_simulation;

    public UnityEngine.UI.Button btn_trees;
    public UnityEngine.UI.Button btn_setup;
}
[System.Serializable]
public class UIManager_Properties_Panel_Front_Neighborhood
{
    public GameObject panel_trees;
    public GameObject panel_setup;

    public UnityEngine.UI.Button btn_trees;
    public UnityEngine.UI.Button btn_setup;
}

[System.Serializable]
public class UIManager_Properties_Panel_Back
{
    public GameObject panel;
    public UnityEngine.UI.Button btn_individual_house;
    public UnityEngine.UI.Button btn_side_by_side;
    public UnityEngine.UI.Button btn_community;
    public UnityEngine.UI.Button btn_neighborhood;
}
[System.Serializable]
public class UIManager_Properties_Panel_Front
{
    public GameObject panel;
    public GameObject panel_individual_house;
    public GameObject panel_side_by_side;
    public GameObject panel_community;
    public GameObject panel_neighborhood;

    public UIManager_Properties_Panel_Front_Individual_House individual_house = new UIManager_Properties_Panel_Front_Individual_House();
    public UIManager_Properties_Panel_Front_Side_By_Side side_by_side = new UIManager_Properties_Panel_Front_Side_By_Side();
    public UIManager_Properties_Panel_Front_Community community = new UIManager_Properties_Panel_Front_Community();
    public UIManager_Properties_Panel_Front_Neighborhood neighborhood = new UIManager_Properties_Panel_Front_Neighborhood();

    public UIManager_Properties_Panel_Mitigate_Individual_House mitigate_individual_house = new UIManager_Properties_Panel_Mitigate_Individual_House();
    public UIManager_Properties_Panel_Setup_Community setup_community = new UIManager_Properties_Panel_Setup_Community();
    public UIManager_Properties_Panel_Setup_Neighborhood setup_neighborhood = new UIManager_Properties_Panel_Setup_Neighborhood();
}

[System.Serializable]
public class UIManager_Properties
{
    public Color button_color_active, button_color_inactive;

    public UIManager_Properties_Panel_Back back = new UIManager_Properties_Panel_Back();
    public UIManager_Properties_Panel_Front front = new UIManager_Properties_Panel_Front();
    public UIManager_Properties_Prompt prompt = new UIManager_Properties_Prompt();

    public UIManager_Properties_Stages stages = new UIManager_Properties_Stages();
}


public class UIManager : UMonoBehavior {

    public UIManager_Properties properties = new UIManager_Properties();

    protected override void Start_1()
    {
        base.Start_1();

        properties.back.panel.SetActive(true);
        properties.front.panel.SetActive(true);

        //CALLBACK_select_major_individual_house();
        {
            properties.front.panel_individual_house.SetActive(false);
            properties.front.panel_side_by_side.SetActive(false);
            properties.front.panel_community.SetActive(false);
            properties.front.panel_neighborhood.SetActive(false);


            button_color_active(properties.back.btn_individual_house, false);
            button_color_active(properties.back.btn_side_by_side, false);
            button_color_active(properties.back.btn_community, false);
            button_color_active(properties.back.btn_neighborhood, false);

            //properties.stages.individual_house.gameObject.SetActive(false);
            //properties.stages.sbs_left.gameObject.SetActive(false);
            //properties.stages.sbs_right.gameObject.SetActive(false);

            button_color_active(properties.back.btn_individual_house, true);
            properties.front.panel_individual_house.SetActive(true);

            properties.stages.individual_house.gameObject.SetActive(true);
        }
    }

    #region button

    void button_color_active(UnityEngine.UI.Button button, bool value)
    {
        var colors = button.colors;
        var text = button.transform.GetComponentInChildren<UnityEngine.UI.Text>();

        //
        if (value)
        {
            colors.normalColor = properties.button_color_active;
            colors.highlightedColor = properties.button_color_active;
            text.color = Color.white;
        }
        else
        {
            colors.normalColor = properties.button_color_inactive;
            colors.highlightedColor = properties.button_color_inactive;
            text.color = Color.black;
        }

        //
        button.colors = colors;
    }

    #endregion

    #region major

    void disable_all_major()
    {
        properties.front.panel_individual_house.SetActive(false);
        properties.front.panel_side_by_side.SetActive(false);
        properties.front.panel_community.SetActive(false);
        properties.front.panel_neighborhood.SetActive(false);


        button_color_active(properties.back.btn_individual_house, false);
        button_color_active(properties.back.btn_side_by_side, false);
        button_color_active(properties.back.btn_community, false);
        button_color_active(properties.back.btn_neighborhood, false);

        properties.stages.individual_house.gameObject.SetActive(false);
        properties.stages.sbs_left.gameObject.SetActive(false);
        properties.stages.sbs_right.gameObject.SetActive(false);
    }

    public void CALLBACK_select_major_individual_house()
    {
        disable_all_major();
        button_color_active(properties.back.btn_individual_house, true);
        properties.front.panel_individual_house.SetActive(true);

        properties.stages.individual_house.gameObject.SetActive(true);
    }
    public void CALLBACK_select_major_side_by_side()
    {
        disable_all_major();
        button_color_active(properties.back.btn_side_by_side, true);
        properties.front.panel_side_by_side.SetActive(true);

        properties.stages.sbs_left.gameObject.SetActive(true);
        properties.stages.sbs_right.gameObject.SetActive(true);
    }
    public void CALLBACK_select_major_community()
    {
        disable_all_major();
        button_color_active(properties.back.btn_community, true);
        properties.front.panel_community.SetActive(true);
    }
    public void CALLBACK_select_major_neighborhood()
    {
        disable_all_major();
        button_color_active(properties.back.btn_neighborhood, true);
        properties.front.panel_neighborhood.SetActive(true);
    }

    #endregion

    #region minor

    #region individual house
    
    void disable_all_minor_individual_house()
    {
        properties.front.individual_house.panel_trees.SetActive(false);
        properties.front.individual_house.panel_siding.SetActive(false);
        properties.front.individual_house.panel_roof.SetActive(false);
        properties.front.individual_house.panel_starting_objects.SetActive(false);

        button_color_active(properties.front.individual_house.btn_trees, false);
        button_color_active(properties.front.individual_house.btn_siding, false);
        button_color_active(properties.front.individual_house.btn_roof, false);
        button_color_active(properties.front.individual_house.btn_starting_objects, false);
    }

    public void CALLBACK_select_minor_individual_house_trees()
    {
        disable_all_minor_individual_house();
        button_color_active(properties.front.individual_house.btn_trees, true);
        properties.front.individual_house.panel_trees.SetActive(true);
    }
    public void CALLBACK_select_minor_individual_house_siding()
    {
        disable_all_minor_individual_house();
        button_color_active(properties.front.individual_house.btn_siding, true);
        properties.front.individual_house.panel_siding.SetActive(true);
    }
    public void CALLBACK_select_minor_individual_house_roof()
    {
        disable_all_minor_individual_house();
        button_color_active(properties.front.individual_house.btn_roof, true);
        properties.front.individual_house.panel_roof.SetActive(true);
    }
    public void CALLBACK_select_minor_individual_house_starting_objects()
    {
        disable_all_minor_individual_house();
        button_color_active(properties.front.individual_house.btn_starting_objects, true);
        properties.front.individual_house.panel_starting_objects.SetActive(true);
    }

    public void CALLBACK_zone_indicator()
    {
        properties.front.mitigate_individual_house.bZoneIndicator = !properties.front.mitigate_individual_house.bZoneIndicator;
        int value = properties.front.mitigate_individual_house.bZoneIndicator ? 1 : 0;
        Simulation_Persisted_Settings.Instance().set_value_int(ESimulated_Persisted_Settings_Mode.Individual_House, 
            Simulation_Persisted_Settings.Instance().DEFINE_KEY_ZONE_INDICATOR, 
            value);
    }

    #endregion

    #region community

    void disable_all_minor_community()
    {
        properties.front.community.panel_simulation.SetActive(false);
        properties.front.community.panel_setup.SetActive(false);

        button_color_active(properties.front.community.btn_trees, false);
        button_color_active(properties.front.community.btn_setup, false);
    }

    public void CALLBACK_select_minor_community_trees()
    {
        //disable_all_minor_community();
        //button_color_active(properties.front.community.btn_trees, true);
        //properties.front.community.panel_trees.SetActive(true);
    }
    public void CALLBACK_select_minor_community_setup()
    {
        disable_all_minor_community();
        button_color_active(properties.front.community.btn_setup, true);
        properties.front.community.panel_setup.SetActive(true);
    }
    #endregion

    #region neighborhood

    void disable_all_minor_neighborhood()
    {
        properties.front.neighborhood.panel_trees.SetActive(false);
        properties.front.neighborhood.panel_setup.SetActive(false);

        button_color_active(properties.front.neighborhood.btn_trees, false);
        button_color_active(properties.front.neighborhood.btn_setup, false);
    }

    public void CALLBACK_select_minor_neighborhood_trees()
    {
        disable_all_minor_neighborhood();
        button_color_active(properties.front.neighborhood.btn_trees, true);
        properties.front.neighborhood.panel_trees.SetActive(true);
    }
    public void CALLBACK_select_minor_neighborhood_setup()
    {
        disable_all_minor_neighborhood();
        button_color_active(properties.front.neighborhood.btn_setup, true);
        properties.front.neighborhood.panel_setup.SetActive(true);
    }
    
    #endregion

    #endregion

    #region button mitigate/start fire/setup

    #region mitigate

    public void CALLBACK_mitigate_individual_house()
    {
        properties.back.panel.SetActive(false);
        properties.front.panel.SetActive(false);
        properties.front.mitigate_individual_house.panel.SetActive(true);
        mitigate_individual_house_maximize_options();
    }

    #endregion

    #region setup

    public void CALLBACK_setup_community()
    {
        properties.front.panel_community.SetActive(false);
        properties.front.setup_community.panel.SetActive(true);
        setup_community_maximize_options();
    }
    public void CALLBACK_setup_neighborhood()
    {
        properties.front.panel_neighborhood.SetActive(false);
        properties.front.setup_neighborhood.panel.SetActive(true);
        setup_neighborhood_maximize_options();
    }

    #endregion

    #region start fire

    //public void CALLBACK_start_fire_individual_house() { }
    //public void CALLBACK_start_fire_community() { }
    //public void CALLBACK_start_fire_neighborhood() { }

    #endregion

    #endregion

    #region mitigate individual house


    void mitigate_individual_house_state_1()
    {

        properties.front.mitigate_individual_house.panel_state_1.SetActive(true);
        properties.front.mitigate_individual_house.panel_state_2.SetActive(false);
    }
    void mitigate_individual_house_state_2()
    {
        WorldPauseState.Instance().properties.bPaused = false;

        properties.front.mitigate_individual_house.panel_state_1.SetActive(false);
        properties.front.mitigate_individual_house.panel_state_2.SetActive(true);

        properties.front.mitigate_individual_house.panel_options_maximized.SetActive(false);
        properties.front.mitigate_individual_house.panel_options_minimized.SetActive(false);


        properties.stages.individual_house_CLONE = Instantiate<StageReference>(properties.stages.individual_house);
        //properties.stages.individual_house.gameObject.SetActive(false);
        properties.stages.disable_originals();
        properties.stages.individual_house_CLONE.transform.parent = properties.stages.individual_house.transform.parent;
        properties.stages.individual_house_CLONE.transform.position = properties.stages.individual_house.transform.position;
        properties.stages.individual_house_CLONE.properties.fires.SetActive(true);

    }

    void mitigate_individual_house_maximize_options()
    {
        properties.front.mitigate_individual_house.panel_options_maximized.SetActive(true);
        properties.front.mitigate_individual_house.panel_options_minimized.SetActive(false);
    }
    void mitigate_individual_house_minimize_options()
    {
        properties.front.mitigate_individual_house.panel_options_maximized.SetActive(false);
        properties.front.mitigate_individual_house.panel_options_minimized.SetActive(true);
    }

    public void CALLBACK_mitigate_individual_house_start_fire()
    {
        if (is_prompt_blocking_input())
            return;

        mitigate_individual_house_state_2();

    }
    public void CALLBACK_mitigate_individual_house_maximize_options()
    {
        if (is_prompt_blocking_input())
            return; 
        mitigate_individual_house_maximize_options();
    }
    public void CALLBACK_mitigate_individual_house_minimize_options()
    {
        if (is_prompt_blocking_input())
            return; 
        mitigate_individual_house_minimize_options();
    }
    public void CALLBACK_mitigate_individual_house_play()
    {
        if (is_prompt_blocking_input())
            return; 
        //button_color_active(properties.front.mitigate_individual_house.btn_play, true);
        //button_color_active(properties.front.mitigate_individual_house.btn_pause, false);

        WorldPauseState.Instance().properties.bPaused = false;
    }
    public void CALLBACK_mitigate_individual_house_pause()
    {
        if (is_prompt_blocking_input())
            return; 
        //button_color_active(properties.front.mitigate_individual_house.btn_play, false);
        //button_color_active(properties.front.mitigate_individual_house.btn_pause, true);

        WorldPauseState.Instance().properties.bPaused = true;
    }
    public void CALLBACK_mitigate_individual_house_back()
    {
        if (is_prompt_blocking_input())
            return;

        //
        raise_prompt(succeed_mitigate_individual_house_back);
    }
    public void CALLBACK_mitigate_individual_house_zone_indicator()
    {
        if (is_prompt_blocking_input())
            return; 
    }

    //
    void succeed_mitigate_individual_house_back()
    {

        //
        if (properties.stages.individual_house_CLONE != null)
            Destroy(properties.stages.individual_house_CLONE.gameObject);
        //properties.stages.individual_house.gameObject.SetActive(true);
        properties.stages.enable_originals();

        //
        properties.back.panel.SetActive(true);
        properties.front.panel.SetActive(true);
        properties.front.panel_individual_house.SetActive(true);

        //
        mitigate_individual_house_state_1();
        properties.front.mitigate_individual_house.panel.SetActive(false);
    }

    #endregion

    #region side by side - start_fire / back / play / pause

    public void CALLBACK_start_fire_side_by_side()
    {
        WorldPauseState.Instance().properties.bPaused = false;

        properties.front.side_by_side.panel_settings.SetActive(false);
        properties.front.side_by_side.panel_simulate.SetActive(true);

        properties.stages.sbs_left_CLONE = Instantiate<StageReference>(properties.stages.sbs_left);
        //properties.stages.sbs_left.gameObject.SetActive(false);
        properties.stages.sbs_left_CLONE.transform.parent = properties.stages.sbs_left.transform.parent;
        properties.stages.sbs_left_CLONE.transform.position = properties.stages.sbs_left.transform.position;
        properties.stages.sbs_left_CLONE.properties.fires.SetActive(true);

        properties.stages.sbs_right_CLONE = Instantiate<StageReference>(properties.stages.sbs_right);
        //properties.stages.sbs_right.gameObject.SetActive(false);
        properties.stages.sbs_right_CLONE.transform.parent = properties.stages.sbs_right.transform.parent;
        properties.stages.sbs_right_CLONE.transform.position = properties.stages.sbs_right.transform.position;
        properties.stages.sbs_right_CLONE.properties.fires.SetActive(true);


        properties.stages.disable_originals();
    }
    public void CALLBACK_simulate_side_by_side_back()
    {
        if (is_prompt_blocking_input())
            return;

        //
        raise_prompt(succeed_simulate_side_by_side_back);
    }
    void succeed_simulate_side_by_side_back()
    {
        properties.front.side_by_side.panel_settings.SetActive(true);
        properties.front.side_by_side.panel_simulate.SetActive(false);

        //
        if (properties.stages.sbs_left_CLONE != null)
            Destroy(properties.stages.sbs_left_CLONE.gameObject);
        //properties.stages.sbs_left.gameObject.SetActive(true);

        //
        if (properties.stages.sbs_right_CLONE != null)
            Destroy(properties.stages.sbs_right_CLONE.gameObject);
        //properties.stages.sbs_right.gameObject.SetActive(true);

        properties.stages.enable_originals();

        //
        properties.back.panel.SetActive(true);
        properties.front.panel.SetActive(true);
        properties.front.panel_side_by_side.SetActive(true);
    }
    public void CALLBACK_simulate_side_by_side_play()
    {
        if (is_prompt_blocking_input())
            return;

        WorldPauseState.Instance().properties.bPaused = false;
    }
    public void CALLBACK_simulate_side_by_side_pause()
    {
        if (is_prompt_blocking_input())
            return;

        WorldPauseState.Instance().properties.bPaused = true;
    }

    #endregion

    #region setup community


    void setup_community_state_1()
    {
        properties.front.setup_community.panel_state_1.SetActive(true);
        properties.front.setup_community.panel_state_2.SetActive(false);
    }
    void setup_community_state_2()
    {
        properties.front.setup_community.panel_state_1.SetActive(false);
        properties.front.setup_community.panel_state_2.SetActive(true);
    }

    void setup_community_maximize_options()
    {
        properties.front.setup_community.panel_options_maximized.SetActive(true);
        properties.front.setup_community.panel_options_minimized.SetActive(false);
    }
    void setup_community_minimize_options()
    {
        properties.front.setup_community.panel_options_maximized.SetActive(false);
        properties.front.setup_community.panel_options_minimized.SetActive(true);
    }

    public void CALLBACK_setup_community_start_fire()
    {
        if (is_prompt_blocking_input())
            return;

        WorldPauseState.Instance().properties.bPaused = false;

        setup_community_state_2();

        properties.stages.community_CLONE = Instantiate<StageReference>(properties.stages.community);
        //properties.stages.community.gameObject.SetActive(false);
        properties.stages.disable_originals();
        properties.stages.community_CLONE.transform.parent = properties.stages.community.transform.parent;
        properties.stages.community_CLONE.transform.position = properties.stages.community.transform.position;
        properties.stages.community_CLONE.properties.fires.SetActive(true);
    }
    public void CALLBACK_setup_community_maximize_options()
    {
        if (is_prompt_blocking_input())
            return; 
        setup_community_maximize_options();
    }
    public void CALLBACK_setup_community_minimize_options()
    {
        if (is_prompt_blocking_input())
            return; 
        setup_community_minimize_options();
    }
    public void CALLBACK_setup_community_play()
    {
        if (is_prompt_blocking_input())
            return; 
        button_color_active(properties.front.setup_community.btn_play, true);
        button_color_active(properties.front.setup_community.btn_pause, false);
    }
    public void CALLBACK_setup_community_pause()
    {
        if (is_prompt_blocking_input())
            return; 
        button_color_active(properties.front.setup_community.btn_play, false);
        button_color_active(properties.front.setup_community.btn_pause, true);
    }
    public void CALLBACK_setup_community_back()
    {
        if (is_prompt_blocking_input())
            return;
        raise_prompt(succeed_setup_community_back);
    }

    void succeed_setup_community_back()
    {

        properties.front.setup_community.panel.SetActive(false);
        properties.front.panel_community.SetActive(true);

        //
        if (properties.stages.community_CLONE != null)
            Destroy(properties.stages.community_CLONE.gameObject);
        //properties.stages.community.gameObject.SetActive(true);

        properties.stages.enable_originals();

        //
        properties.back.panel.SetActive(true);
        properties.front.panel.SetActive(true);
    }

    #endregion

    #region setup neighborhood


    void setup_neighborhood_state_1()
    {
        properties.front.setup_neighborhood.panel_state_1.SetActive(true);
        properties.front.setup_neighborhood.panel_state_2.SetActive(false);
    }
    void setup_neighborhood_state_2()
    {
        properties.front.setup_neighborhood.panel_state_1.SetActive(false);
        properties.front.setup_neighborhood.panel_state_2.SetActive(true);
    }

    void setup_neighborhood_maximize_options()
    {
        properties.front.setup_neighborhood.panel_options_maximized.SetActive(true);
        properties.front.setup_neighborhood.panel_options_minimized.SetActive(false);
    }
    void setup_neighborhood_minimize_options()
    {
        properties.front.setup_neighborhood.panel_options_maximized.SetActive(false);
        properties.front.setup_neighborhood.panel_options_minimized.SetActive(true);
    }

    public void CALLBACK_setup_neighborhood_start_fire()
    {
        if (is_prompt_blocking_input())
            return;
        WorldPauseState.Instance().properties.bPaused = false;

        setup_neighborhood_state_2();

        properties.stages.neighborhood_CLONE = Instantiate<StageReference>(properties.stages.neighborhood);
        //properties.stages.neighborhood.gameObject.SetActive(false);
        properties.stages.disable_originals();
        properties.stages.neighborhood_CLONE.transform.parent = properties.stages.neighborhood.transform.parent;
        properties.stages.neighborhood_CLONE.transform.position = properties.stages.neighborhood.transform.position;
        properties.stages.neighborhood_CLONE.properties.fires.SetActive(true);
    }
    public void CALLBACK_setup_neighborhood_maximize_options()
    {
        if (is_prompt_blocking_input())
            return; 
        setup_neighborhood_maximize_options();
    }
    public void CALLBACK_setup_neighborhood_minimize_options()
    {
        if (is_prompt_blocking_input())
            return; 
        setup_neighborhood_minimize_options();
    }
    public void CALLBACK_setup_neighborhood_play()
    {
        if (is_prompt_blocking_input())
            return; 
        button_color_active(properties.front.setup_neighborhood.btn_play, true);
        button_color_active(properties.front.setup_neighborhood.btn_pause, false);
    }
    public void CALLBACK_setup_neighborhood_pause()
    {
        if (is_prompt_blocking_input())
            return; 
        button_color_active(properties.front.setup_neighborhood.btn_play, false);
        button_color_active(properties.front.setup_neighborhood.btn_pause, true);
    }
    public void CALLBACK_setup_neighborhood_back()
    {
        if (is_prompt_blocking_input())
            return;
        raise_prompt(succeed_setup_neighborhood_back);
    }

    public void succeed_setup_neighborhood_back()
    {
        properties.front.setup_neighborhood.panel.SetActive(false);
        properties.front.panel_neighborhood.SetActive(true);

        //
        if (properties.stages.neighborhood_CLONE != null)
            Destroy(properties.stages.neighborhood_CLONE.gameObject);
        //properties.stages.neighborhood.gameObject.SetActive(true);
        properties.stages.enable_originals();

        //
        properties.back.panel.SetActive(true);
        properties.front.panel.SetActive(true);
    }

    #endregion

    #region prompt

    delegate void Delegate_Prompt_Yes();
    Delegate_Prompt_Yes dPromptYes;

    bool is_prompt_blocking_input() { return properties.prompt.panel.activeInHierarchy; }

    void raise_prompt(Delegate_Prompt_Yes dyes)
    {
        dPromptYes = dyes;
        properties.prompt.panel.SetActive(true);
    }
    void lower_prompt()
    {
        properties.prompt.panel.SetActive(false);
    }

    public void CALLBACK_set_prompt_answer_yes()
    {
        if (dPromptYes != null)
            dPromptYes();
        dPromptYes = null;

        //
        lower_prompt();
    }
    public void CALLBACK_set_prompt_answer_no()
    {
        lower_prompt();
    }

    #endregion

    #region toggle

    #region individual house

    public void CALLBACK_toggle_front_individual_house_trees()
    {
        int value = 0;
        foreach (Transform child in properties.front.individual_house.parent_toggle_trees.transform)
        {
            UnityEngine.UI.Toggle tog = child.GetComponent<UnityEngine.UI.Toggle>();
            value = tog.isOn ? 1 : 0;
            Simulation_Persisted_Settings.Instance().set_value_int(ESimulated_Persisted_Settings_Mode.Individual_House, tog.name, value);
        }
    }

    #endregion

    #endregion
}
