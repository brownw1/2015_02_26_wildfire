﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class UFakeRenderTexture_Properties
{
    public Texture2D texture;
    public float updateFrequency = .5f;
}

public class UFakeRenderTexture : UMonoBehavior {

    public UFakeRenderTexture_Properties properties = new UFakeRenderTexture_Properties();
    private float lastUpdate = 0f;
    private Rect sourceRect;

    void Start()
    {
        sourceRect = new Rect(0, 0, properties.texture.width, properties.texture.height);
        GetComponent<Camera>().pixelRect = sourceRect;
    }

    void OnPostRender()
    {
        if (Time.time > lastUpdate + properties.updateFrequency)
        {
            lastUpdate = Time.time;
            properties.texture.ReadPixels(sourceRect, 0, 0);
            properties.texture.Apply();
        }
    }
}
