﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections;


public enum ULogger_Title{
	
	SYSTEM,
	
	GAMELOGIC,

    SOCIAL,
	
	NETWORKING,
	NETWORKCLIENT,
	NETWORKSERVER,
	
	CONTENT,
	
	TERRAIN,
	
	INPUT,
	
	AUDIO,
	
	PHYSICS,
	
	GUI,
	
	AI,
	
	MISC,
	WARNING,
	ERROR
}

[System.Serializable]
public class ULogger_Properties
{
    public bool m_enabled = true;
    public bool m_screen = false;
}

public class ULogger : USingleton_Game<ULogger> {

    //
    public ULogger_Properties m_properties = new ULogger_Properties();
	
	//
    public bool Write_Enabled { get { return m_properties.m_enabled; } set { m_properties.m_enabled = value; } }
    public bool Write_To_Screen { get { return m_properties.m_screen; } set { m_properties.m_screen = value; } }


    #region start/update

    //
    protected override void Awake_1()
    {


        //  Create our directories if they dont exist.
        //
        if (!System.IO.Directory.Exists(DEFINE_INDEX_PATH))
            System.IO.Directory.CreateDirectory(DEFINE_INDEX_PATH);
        if (!System.IO.Directory.Exists(DEFINE_IMAGES_PATH))
            System.IO.Directory.CreateDirectory(DEFINE_IMAGES_PATH);

        //
        PlayerPrefs.SetInt(DEFINE_PLAYERPREFS_KEY_LOGGER_INDEX, 1);
        PlayerPrefs.SetInt(DEFINE_PLAYERPREFS_KEY_TRACE_INDEX, 1);

        //
        m_filepath = get_html_filepath();

        //
        create_html();

        //
        write(ULogger_Title.SYSTEM, "logger init");
    }
    
    //
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F12))
            write(ULogger_Title.MISC, "manual log", true);
    }

    #endregion

    public void write(ULogger_Title title,string msg, bool screenshot=false){
		
		//
        if (!Write_Enabled)
			return;
		
		//
		string s_title = title_to_string(title);
		
		//
		add_log(s_title,msg,screenshot);
	}

    #region names / paths

    static string DEFINE_PLAYERPREFS_KEY_LOGGER_INDEX { get { return "html logger index"; } }
    static string DEFINE_PLAYERPREFS_KEY_TRACE_INDEX { get { return "html trace index"; } }

    static string DEFINE_INDEX_PATH { get { return string.Format("{0}/../html_logger/", Application.dataPath); } }
    static string DEFINE_IMAGES_PATH { get { return string.Format("{0}/../html_logger/images/", Application.dataPath); } }
    static string DEFINE_IMAGES_LOCAL_PATH { get { return "images/"; } }

    string m_filepath = "";

    string get_logger_filename(string suffix)
    {
        int index = find_log_number();
        string month = System.DateTime.Now.Month.ToString();
        string day = System.DateTime.Now.Day.ToString();
        int ihour = System.DateTime.Now.Hour;
        string daytime = "AM";
        if (ihour > 12)
        {
            daytime = "PM";
            ihour -= 12;
        }
        string hour = "" + ihour;
        string min = System.DateTime.Now.Minute.ToString();
        string second = System.DateTime.Now.Second.ToString();
        string year = System.DateTime.Now.Year.ToString();
        return string.Format("log_v{0}_{1}_{2}_{3}_h{4}_m{5}_s{6}_{7}.{8}", index, month, day, year, hour, min, second, daytime,suffix);
    }
    string get_html_filepath() { return DEFINE_INDEX_PATH + get_logger_filename("html"); }
    void get_screenshot_filepath( out string absolute, out string local) {
        absolute = DEFINE_IMAGES_PATH + get_logger_filename("png");
        local = DEFINE_IMAGES_LOCAL_PATH + get_logger_filename("png");
    }

    #endregion

    #region title

    string title_to_string(ULogger_Title title){
		
		//
		switch(title){
			
		case ULogger_Title.MISC: return DEFINE_TITLE_MISC;
		case ULogger_Title.WARNING: return DEFINE_TITLE_WARNING;
		case ULogger_Title.ERROR: return DEFINE_TITLE_ERROR;
		
		case ULogger_Title.SYSTEM: return DEFINE_TITLE_SYSTEM;
		case ULogger_Title.GAMELOGIC: return DEFINE_TITLE_GAMELOGIC;

        case ULogger_Title.SOCIAL: return DEFINE_TITLE_SOCIAL;
			
		case ULogger_Title.NETWORKING: return DEFINE_TITLE_NETWORKING;
		case ULogger_Title.NETWORKCLIENT: return DEFINE_TITLE_NETWORKCLIENT;
		case ULogger_Title.NETWORKSERVER: return DEFINE_TITLE_NETWORKSERVER;
		
		case ULogger_Title.INPUT: return DEFINE_TITLE_INPUT;
		
		case ULogger_Title.AI: return DEFINE_TITLE_AI;
		case ULogger_Title.AUDIO: return DEFINE_TITLE_AUDIO;
		case ULogger_Title.PHYSICS: return DEFINE_TITLE_PHYSICS;
			
		case ULogger_Title.GUI: return DEFINE_TITLE_GUI;
		
		case ULogger_Title.CONTENT: return DEFINE_TITLE_CONTENT;
			
		case ULogger_Title.TERRAIN: return DEFINE_TITLE_TERRAIN;
			
		}
		
		//
		return ULogger.DEFINE_TITLE_SYSTEM;
	}
	
	#endregion
	
	#region html
	
	static string DEFINE_TITLE_SYSTEM {get{return "System";}}
    static string DEFINE_TITLE_GAMELOGIC { get { return "Gamelogic"; } }
    static string DEFINE_TITLE_SOCIAL { get { return "Social"; } }
    static string DEFINE_TITLE_NETWORKING { get { return "Networking"; } }
	static string DEFINE_TITLE_NETWORKCLIENT {get{return "NetworkClient";}}
	static string DEFINE_TITLE_NETWORKSERVER {get{return "NetworkServer";}}
	static string DEFINE_TITLE_AUDIO {get{return "Audio";}}
	static string DEFINE_TITLE_PHYSICS {get{return "Physics";}}
	static string DEFINE_TITLE_TERRAIN {get{return "Terrain";}}
	static string DEFINE_TITLE_AI {get{return "Ai";}}
	static string DEFINE_TITLE_MISC {get{return "Misc";}}
	static string DEFINE_TITLE_WARNING {get{return "Warning";}}
	static string DEFINE_TITLE_ERROR {get{return "Error";}}
	static string DEFINE_TITLE_GUI {get{return "GUI";}}
	static string DEFINE_TITLE_INPUT {get{return "Input";}}
	static string DEFINE_TITLE_CONTENT {get{return "Content";}}
	
	string get_timestamp(){ return System.DateTime.Now.ToString("[MM/dd/yyyy] [HH:mm:ss] [tt]"); }
	
	string add_button(string html, string title){
		string button = string.Format("<input type=\"button\" value=\"{0}\" class=\"{0} Button\" onclick=\"hide_class('{0}')\"/>   ",title);
		return html + button;
	}
	string add_header(string html){
		
		//
		string platform = Application.platform.ToString();
		string timestmap = get_timestamp();
		
		//
		string header = string.Format("<h1 class=\"date\">{0} {1}</h1>",timestmap,platform);
		
		
		//
		return html + header;
	}

    int find_log_number()
    {
        if (!PlayerPrefs.HasKey(DEFINE_PLAYERPREFS_KEY_LOGGER_INDEX))
        {
            PlayerPrefs.SetInt(DEFINE_PLAYERPREFS_KEY_LOGGER_INDEX, 1);
            return 1;
        }
        return PlayerPrefs.GetInt(DEFINE_PLAYERPREFS_KEY_LOGGER_INDEX);
    }
    int get_increment_trace_number()
    {
        if (!PlayerPrefs.HasKey(DEFINE_PLAYERPREFS_KEY_TRACE_INDEX))
        {
            PlayerPrefs.SetInt(DEFINE_PLAYERPREFS_KEY_TRACE_INDEX, 1);
            return 1;
        }
        int value = PlayerPrefs.GetInt(DEFINE_PLAYERPREFS_KEY_TRACE_INDEX) + 1;
        PlayerPrefs.SetInt(DEFINE_PLAYERPREFS_KEY_TRACE_INDEX, value);
        return value;

    }
	
	void create_html(){
		
		//
		string html = "<script language=\"javascript\" src=\"logger.js\"></script><link rel=\"stylesheet\" type=\"text/css\" href=\"logger.css\" />&nbsp;";
		
		//
		html = add_button(html,DEFINE_TITLE_MISC);
		html = add_button(html,DEFINE_TITLE_WARNING);
		html = add_button(html,DEFINE_TITLE_ERROR);
		html = add_button(html,DEFINE_TITLE_SYSTEM);
		html = add_button(html,DEFINE_TITLE_GAMELOGIC);
		html = add_button(html,DEFINE_TITLE_INPUT);
		html = add_button(html,DEFINE_TITLE_TERRAIN);
		html = add_button(html,DEFINE_TITLE_AI);
		html = add_button(html,DEFINE_TITLE_AUDIO);
		html = add_button(html,DEFINE_TITLE_PHYSICS);
		html = add_button(html,DEFINE_TITLE_GUI);
        html = add_button(html, DEFINE_TITLE_CONTENT);
        html = add_button(html, DEFINE_TITLE_SOCIAL);
        html = add_button(html, DEFINE_TITLE_NETWORKING);
		html = add_button(html,DEFINE_TITLE_NETWORKCLIENT);
		html = add_button(html,DEFINE_TITLE_NETWORKSERVER);
		html += "<br/>";
		
		//
		html = add_header(html);
		
		//	Create.
		//
		using (System.IO.StreamWriter file = new System.IO.StreamWriter(m_filepath))
		{
			file.Write(html);
		}
	}
	
	void add_log(string title, string msg, bool screenshot=false){

        //
        string timestamp = get_timestamp();
        string trace = "trace"+get_increment_trace_number();
		string stack = UnityEngine.StackTraceUtility.ExtractStackTrace();

        //  Write to screen?
        //
        if (Write_To_Screen)
            Debug.Log(string.Format("({0}) >>> {1}",title,msg));
        
        //
        string html = string.Format("<p class=\"{0}\"><span class=\"time\">{1}&nbsp;&nbsp;&nbsp;&nbsp;</span><a onclick=\"hide('{2}')\">STACK</a>&nbsp;&nbsp;&nbsp;&nbsp;{3}</p>", title, timestamp, trace, msg);
        
        //  Stacktrace + Screenshot.
        //
        if (screenshot)
        {
            string ss_filepath, ss_local_filepath;
            get_screenshot_filepath(out ss_filepath, out ss_local_filepath);
            html += string.Format("<pre id={0} class=\"Stack\" style=\"display: none\">{1}<img src=\"{2}\"></pre>", trace, stack, ss_local_filepath);
            UScreenCapture.Instance().write(ss_filepath);
        }

        //  Only Stacktrace.
        //
        else
            html += string.Format("<pre id={0} class=\"Stack\" style=\"display: none\">{1}</pre>", trace, stack);
		
		//	Append.
		//
		using (System.IO.StreamWriter file = new System.IO.StreamWriter(m_filepath, true))
		{
			file.WriteLine(html);
		}
	}
	
	#endregion


}
