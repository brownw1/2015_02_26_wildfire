﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections;

public class UMonoBehavior : MonoBehaviour
{
    private float DEFINE_DELAY_AWAKE2 { get { return 0.1f; } }
    private float DEFINE_DELAY_AWAKE3 { get { return 0.2f; } }

    private float DEFINE_DELAY_START1 { get { return 0.3f; } }
    private float DEFINE_DELAY_START2 { get { return 0.4f; } }
    private float DEFINE_DELAY_START3 { get { return 0.5f; } }

    private uint uid = 0;
    private static uint COUNTER_UID = 0;
    public uint UID { get { return uid; } }

    #region Awake

    // Use this for initialization
    void Awake()
    {
        //
        ++COUNTER_UID;
        uid = COUNTER_UID;

        //
        Awake_1();
        Invoke("Awake_2", DEFINE_DELAY_AWAKE2);
        Invoke("Awake_3", DEFINE_DELAY_AWAKE3);
    }

    /// <summary>
    /// Runs in the 1st wave of awakes.
    /// </summary>
    protected virtual void Awake_1() { }

    /// <summary>
    /// Runs in the 2nd wave of awakes.
    /// </summary>
    protected virtual void Awake_2() { }

    /// <summary>
    /// Runs in the 3rd wave of awakes.
    /// </summary>
    protected virtual void Awake_3() { }

    #endregion

    #region Start

    // Use this for initialization
    void Start()
    {
        Invoke("Start_1", DEFINE_DELAY_START1);
        Invoke("Start_2", DEFINE_DELAY_START2);
        Invoke("Start_3", DEFINE_DELAY_START3);
    }

    /// <summary>
    /// Runs in the 1st wave of starts.
    /// </summary>
    protected virtual void Start_1() { }

    /// <summary>
    /// Runs in the 2nd wave of starts.
    /// </summary>
    protected virtual void Start_2() { }

    /// <summary>
    /// Runs in the 3rd wave of starts.
    /// </summary>
    protected virtual void Start_3() { }

    #endregion

}
