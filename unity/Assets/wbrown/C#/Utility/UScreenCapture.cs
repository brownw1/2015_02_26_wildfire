﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections;

public class UScreenCapture : USingleton_Game<UScreenCapture> {

    //
    public void write(string filepath)
    {
        SaveScreenshot_Capture(filepath);
    }
    void SaveScreenshot_Capture(string filePath)
    {
        Application.CaptureScreenshot(filePath);
    }
}
