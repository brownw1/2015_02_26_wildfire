﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections;

using UnityEngine;
using System.Collections;

/// <summary>
/// Exists singularly (one and only) for the scope of MULTIPLE SCENES.
/// </summary>
/// <typeparam name="T"></typeparam>
public class USingleton_Game<T> : UMonoBehavior where T : UMonoBehavior
{
    protected static T instance;

    /**
       Returns the instance of this singleton.
    */
    public static T Instance()
    {
        if (instance == null)
        {
            instance = (T)FindObjectOfType(typeof(T));

            if (instance == null)
            {
                string msg = "An instance of " + typeof(T) +
                   " is needed in the scene, but there is none.";
                ULogger.Instance().write(ULogger_Title.ERROR, msg);
                return null;
            }
        }

        //
        DontDestroyOnLoad(instance);

        //
        return instance;
    }
}