﻿/*******************************************************
 * Copyright (C) 2015 Wallace Brown <brownwcontact@gmail.com>
 *******************************************************/

using UnityEngine;
using System.Collections;

public class UVector {

    Vector4 vector = new Vector4();
    public Vector4 Value { get { return vector; } set { vector = value; } }
    public void Set(float value) { vector.x = value; vector.y = value; vector.z = value; vector.w = value; }
    public void Set(float x, float y) { vector.x = x; vector.y = y; }
    public void Set(float x, float y, float z) { vector.x = x; vector.y = y; vector.z = z; }
    public void Set(float x, float y, float z, float w) { vector.x = x; vector.y = y; vector.z = z; vector.w = w; }

    public static UVector zero { get { return new UVector(); } }

    public UVector() { vector = Vector4.zero; }
    public UVector(float value) { vector.x = value; vector.y = value; vector.z = value; vector.w = value; }
    public UVector(float x, float y) { vector.x = x; vector.y = y; }
    public UVector(float x, float y, float z) { vector.x = x; vector.y = y; vector.z = z; }
    public UVector(float x, float y, float z, float w) { vector.x = x; vector.y = y; vector.z = z; vector.w = w; }
}
