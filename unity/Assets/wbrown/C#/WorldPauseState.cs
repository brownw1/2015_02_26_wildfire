﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class WorldPauseState_Properties
{
    public bool bPaused = false;
}
public class WorldPauseState : USingleton_Scene<WorldPauseState>
{
    public WorldPauseState_Properties properties = new WorldPauseState_Properties();


}
